package com.java.test;

public class User {

    private String username;
    private Role role;
    private String adres;
    private String PCip;

    public String getPCip() {
        return PCip;
    }

    public void setPCip(String PCip) {
        this.PCip = PCip;
    }

    public User(String name, Role role, String adres, String pCip) {
        this.name = name;
        this.role = role;
        this.adres = adres;
        this.PCip = setDefault(pCip);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private boolean isCorrect(String pcId) {
        return pcId.length()==7;
    }
    private String setDefault(String pcId){
        if(isCorrect(pcId)) {
            return pcId;
        }else {
            return "00xx00FD";
        }
    }
}
